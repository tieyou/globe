import React from 'react';
import {
  AppRegistry,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import FooScreen from './src/components/foo/FooScreen'
import ChatScreen from './src/components/chat/ChatScreen'
import HomeScreen from './src/components/home/HomeScreen'

const Globe = StackNavigator({
  Home: { screen: HomeScreen },
  Foo: { screen: FooScreen },
  Chat: { screen: ChatScreen },
});


AppRegistry.registerComponent('Globe', () => Globe);
