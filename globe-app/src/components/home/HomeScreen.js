import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Button,
  StatusBar
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import {OverviewComponent} from '../overview/OverviewComponent'


export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
    headerStyle:{
      backgroundColor:'red'
    }
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <StatusBar
          hidden={false}
          backgroundColor='blue'
        />
        <Text>Hello, Chat App!</Text>
        <Button
          onPress={() => navigate('Foo')}
          title="Chat with Lucy"
        />
        <OverviewComponent>
        </OverviewComponent>
      </View>

    );
  }
}
