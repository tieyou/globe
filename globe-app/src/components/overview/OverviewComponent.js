/**
* Created by thum on 12.06.17.
*/


import React, {Component} from 'react';
import {
  ListView,
  Text,
} from 'react-native';

export class OverviewComponent extends React.Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2','row 3']),
    };
  }

  getInitialState() {
    return {
      jsonURL: 'http://127.0.0.1:8000/api/products.json',
      dataSource: ds.cloneWithRows(['row 1', 'row 2','row 4']),
    }
  }

  render() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={(data) => <Text>{data}</Text>}
      />
    );
  }

  componentDidMount(){
    console.debug("---debug---");
    console.info("---info---");
    console.warn("---warn---")
    this.loadJSONData();
  }

  loadJSONData() {

    fetch("http://127.0.0.1:8000/api/products.json", {method: "GET"})
    .then((response) => response.json())
    .then((responseData) =>
    {
      console.debug("---debug---"+responseData[0]["name"]);
      this.setState({dataSource:this.state.dataSource.cloneWithRowsAndSections(responseData)})
    })
    .done(() => {
    });

  }

}
