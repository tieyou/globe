<?php

namespace GlobeBundle\Controller;

use GlobeBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GlobeController extends Controller
{

    public function getProductAction(Request $request){
        $params = $request->query->all();
        $name = $params['name'];
        $product = $this->getDoctrine()->getRepository('GlobeBundle:Product')->findOneByName($name);
        if(!is_object($product)){
            throw $this->createNotFoundException();
        }
        return $product;
    }

    public function getProductsAction(Request $request){
        $params = $request->query->all();
        $products = $this->getDoctrine()->getRepository('GlobeBundle:Product')->findBy($params);
        return $products;
    }

    public function postProductsAction(Request $request){
        $params = $request->request->all();
        $price = $params['price'];
        $name = $params['name'];

        $product = new Product();
        $product->setName($name);
        $product->setPrice($price);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($product);
        $manager->flush();
    }

}
